<div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" value="{{ $character->name }}" name="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            {{ $character->image }}
            <img src="{{ asset('storage').'/'.$character->image }}" alt="Fotografía">
            <input type="file" value="" name="image">    
         </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Status">Status:</label>
              <input type="text" class="form-control" value="{{ $character->status }}" name="status">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Species">Species:</label>
              <input type="text" class="form-control" value="{{ $character->species }}" name="species">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Type">Type:</label>
              <input type="text" class="form-control" value="{{ $character->type }}" name="type">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Gender">Gender:</label>
              <input type="text" class="form-control" value="{{ $character->gender }}" name="gender">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Origin">Origin:</label>
              <input type="text" class="form-control" value="{{ $character->origin }}" name="origin">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Location">Location:</label>
              <input type="text" class="form-control" value="{{ $character->location }}" name="location">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Episode">Episode:</label>
              <input type="text" class="form-control" value="{{ $character->episode }}" name="episode">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Url">Url:</label>
              <input type="text" class="form-control" value="{{ $character->url }}" name="url">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Created">Created:</label>
              <input type="text" class="form-control" value="{{ $character->created }}" name="created">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Guardar</button>
          </div>
        </div>
