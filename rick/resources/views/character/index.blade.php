<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> R & M </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>  
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Image</th>
        <th>Status</th>
        <th>Species</th>
        <th>Type</th>
        <th>Gender</th>
        <th>Origin</th>
        <th>Location</th>
        <th>Episode</th>
        <th>URL</th>
        <th>Created</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($characters as $character)
      @php
        $date=date('Y-m-d', $character['date']);
        @endphp
      <tr>
        <td>{{$character->id}}</td>
        <td>{{$character->name}}</td>
        <td>
          <img src="{{ asset('storage').'/'.$character->image }}" alt="Fotografía">
          {{$character->image}}
        </td>
        <td>{{$character->status}}</td>
        <td>{{$character->species}}</td>
        <td>{{$character->type}}</td>
        <td>{{$character->gender}}</td>
        <td>{{$character->origin}}</td>
        <td>{{$character->location}}</td>
        <td>{{$character->episode}}</td>
        <td>{{$character->url}}</td>
        <td>{{$character->created}}</td>
        <td>
          
        <a href="{{ url('/character/'.$character->id.'/edit') }}">Guardar</a> 
        
        | 

          <form action="{{url('/character/'.$character->id)}}" method="post">
            @csrf
            {{ method_field('DELETE') }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Eliminar</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>